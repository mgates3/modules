Docs:
http://modules.sourceforge.net/man/modulefile.html

This directory should be installed as $HOME/privatemodules

See `<package>/.modulerc` for package's default version.

On macOS, I use Homebrew (https://brew.sh/) to install:

    brew install modules

In csh, add to .cshrc file:

    setenv module_script /usr/local/Modules/init/tcsh  # from source
    if (! -f $module_script) then
        setenv module_script /usr/local/opt/modules/init/tcsh  # from homebrew
    endif
    if (-f $module_script) then
        source $module_script
        module load use.own
        source $HOME/.modules
    else
        #echo "module not installed"
    endif

In sh (bash), add to .profile file:

    export module_script=/usr/local/Modules/init/sh  # from source
    if [ ! -f $module_script ]; then
        export module_script=/usr/local/opt/modules/init/sh  # from homebrew
    fi
    if [ -f $module_script ]; then
        source $module_script
        module load use.own
        source $HOME/.modules
    else
        #echo "module not installed"
    fi

where $HOME/.modules has whatever modules you want loaded, e.g.:

    module load cmake
